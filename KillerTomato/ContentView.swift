//
//  ContentView.swift
//  KillerTomato
//
//  Created by FultonDaniel on 2020/06/23.
//  Copyright © 2020 FultonDaniel. All rights reserved.
//

import SwiftUI
import Foundation
import UserNotifications

struct ContentView: View {
    @State private var showAlert = false
    static var tomatoDuration: Double = 1200
    static var breakDuration: Double = 300
    static var longBreakDuration: Double = 900
    let insets: EdgeInsets = .init(top: 20, leading: 20, bottom: 20, trailing: 20)
    var body: some View {
        VStack {
            button(title: "Tomato",
                   message: "Stop Coding",
                   duration: ContentView.tomatoDuration)
            button(title: "Break",
                   message: "Break Over",
                   duration: ContentView.breakDuration)
            button(title: "Long Break",
                   message: "Break Over",
                   duration: ContentView.longBreakDuration)
                .alert(isPresented: $showAlert) { () -> Alert in
                    Alert(title: Text("Tomato"), message: Text("Tomato Done."), dismissButton: .default(Text("OK")))
            }
        }
    }
    
    func button(title: String, message: String, duration: Double) -> some View {
        Button(action: {
            self.startTimerForMessage(message, duration: duration)
        }) {
            Text(title)
        }.padding(insets)
    }
    
    fileprivate func startTimerForMessage(_ newMessage: String, duration: Double) {
        DispatchQueue.global().async {
            self.sayMessage(newMessage, duration: duration)
            self.showNotification(message: newMessage, duration: duration)
        }
    }
    
    func sayMessage(_ message: String, duration: Double) {
        DispatchQueue.global().asyncAfter(deadline: .now() + duration) {
            self.showAlert = true
            let _ = self.shell("say \(message)")
        }
        
    }
    
    func shell(_ command: String) -> String {
        let task = Process()
        let pipe = Pipe()

        task.standardOutput = pipe
        task.arguments = ["-c", command]
        task.launchPath = "/bin/bash"
        task.launch()

        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let output = String(data: data, encoding: .utf8)!

        return output
    }
    
    func showNotification(message: String, duration: Double) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound]) { (success, error) in
            if success {
                self.addNotification(message: message, duration: duration)
            } else {
                print(error.debugDescription)
            }
        }
    }
    
    fileprivate func addNotification(message: String, duration: Double) {
        let content = UNMutableNotificationContent()
        content.title = message
        content.subtitle = message
        content.sound = UNNotificationSound.default
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: duration, repeats: false)
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
